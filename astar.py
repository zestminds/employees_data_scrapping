import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
import time

#Seed URL

#Finding URL's on home page and search for staff link
def seedurl():

	#website Info : In Sheet wesbite : 44
	#https://www.a-star.edu.sg/Research/
	#Agency for Science

	#Seed URL 
	url='https://www.a-star.edu.sg/'
	website = requests.get(url)

	website_text = website.text
	links=[]
	soup = BeautifulSoup(website_text, 'lxml')
	print("Processing data. It may take sometime")
	for link in soup.find_all('a'):
		links.append(str(link))	
	
	#Processing the links from the seed URL webpage
	df = pd.DataFrame((links), columns =['Anchorlinks'])
	df[['Anchor', 'Links']] = df['Anchorlinks'].str.split('href="', 1, expand=True)
	df.drop(['Anchorlinks','Anchor'],axis = 'columns',inplace=True)
	df[['Links', 'Titles']] = df['Links'].str.split('">', 1, expand=True)
	df['Titles']=df['Titles'].str.replace("</a>","")
	df["Titles"]=df["Titles"].str.lower()

	#print(df.head(266))

	links_lst=[]

	#Sorting out links with keywords
	searchfor = ['scientists','people', 'staff', 'search','directory','employees','phone','faces','profiles','doctor']
	
	for url_search in soup.find_all('a'):  
		urltext=url_search.get_text().replace(" ", "")		
		for searchtext in searchfor:
			if searchtext in url_search.get_text().lower():			
				try:
					if url_search.get('href') not in links_lst:
						
						if url_search.get('href')==None:
							print('invalid href')
						else :
							links_lst.append(url_search.get('href'))
				except:
					print('invalid href')   
		
	x = "http"
	all_links=[]
	phlinks = []
	for i in range(len(links_lst)):
		
		if x in links_lst[i]:
			continue
		else:
			all_links.append(str(url) + str(links_lst[i]))		

	for nested_url in all_links:		
		page = requests.get(nested_url)
		soup = BeautifulSoup(page.content,'html.parser')
		html_page = str(page.content)
		placeholder = "directory__search"
		if placeholder in html_page:
			phlinks=nested_url
	
	return phlinks


def seleniumDriver():
	options = Options()
	#options.add_argument('--headless')
	options.add_argument('--no-sandbox')
	options.add_argument('--disable-dev-shm-usage')
	options.add_argument("start-maximized")
	options.add_argument('--log-level=1')
	#driver = webdriver.Chrome("C:/Users/michael/Downloads/chromedriver_win32/chromedriver.exe")
	#Or Use
	driver = webdriver.Chrome(chrome_options=options,executable_path='C:/wamp64/www/projects/scrap/chromedriver')
	return driver
	

def makeDataFrame():
	df = pd.DataFrame({'Name':[],'Field of Research': [],'Designation': [],'Entity': [],'Email': []})
	return df

def getEmployees():		
	try:
		driver = seleniumDriver()
		urlsearch=seedurl()
		df=makeDataFrame()
		
		driver.get(urlsearch)
		page_no=0  
		driver.find_element_by_xpath('//*[@id="ContentPlaceHolder_C003_BtnSearch"]').click()		
		time.sleep(10)

	  #--------Data Extraction-------------
		while True:
			if page_no == 0:
				print("page no:" + str(page_no))
				page_no=page_no+1
			else:		
				#Paging 
				try:					
					page_no=page_no+1
					print("page no:" + str(page_no))
					#change Page tag
					element=driver.find_element_by_xpath('//a[contains(string(), "Next")]')
					driver.execute_script("arguments[0].click();", element)
					time.sleep(20)
				except:
					print("No more pages left")
					break
						
			listoflinks=[]
			for row in driver.find_elements_by_css_selector("tr.odd"):
				
				name = row.find_elements_by_tag_name("td")[0]
				fieldofresearch = row.find_elements_by_tag_name("td")[1]
				designation = row.find_elements_by_tag_name("td")[2]
				entity = row.find_elements_by_tag_name("td")[3]
				email = row.find_elements_by_tag_name("td")[4]
				print(name.text)
				df2 = pd.DataFrame({'Name': [name.text], 'Field of Research': [fieldofresearch.text],'Designation': [designation.text],'Entity':[entity.text],'Email':[email.text]})
				df = df.append(df2, ignore_index=True, sort = False)  
				
			for row in driver.find_elements_by_css_selector("tr.even"):
				
				name = row.find_elements_by_tag_name("td")[0]
				fieldofresearch = row.find_elements_by_tag_name("td")[1]
				designation = row.find_elements_by_tag_name("td")[2]
				entity = row.find_elements_by_tag_name("td")[3]
				email = row.find_elements_by_tag_name("td")[4]
				print(name.text)	
				df2 = pd.DataFrame({'Name': [name.text], 'Field of Research': [fieldofresearch.text],'Designation': [designation.text],'Entity':[entity.text],'Email':[email.text]})
				df = df.append(df2, ignore_index=True, sort = False)  	
			
	except:
		print("No more pages left")
		return df
	return df	
		
data=getEmployees()
print('\n')
print(data)
print('\n')
data.to_csv('astar_data.csv')