import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

#Seed URL


def seedurl():

	#website Info : In Sheet wesbite : 14
	#https://www.nist.gov/about-nist/our-organization/people
	#National Institute of Standards and Technology

	#Seed URL 
	#url = 'https://www.anl.gov/staff-directory'
	url='http://www.nist.gov/'
	website = requests.get(url)

	website_text = website.text
	links=[]
	soup = BeautifulSoup(website_text, 'lxml')
	print("Processing data. It may take sometime")
	for link in soup.find_all('a'):
		links.append(str(link))
	#print(links)
	
	#Processing the links from the seed URL webpage
	df = pd.DataFrame((links), columns =['Anchorlinks'])
	df[['Anchor', 'Links']] = df['Anchorlinks'].str.split('href="', 1, expand=True)
	df.drop(['Anchorlinks','Anchor'],axis = 'columns',inplace=True)
	df[['Links', 'Titles']] = df['Links'].str.split('">', 1, expand=True)
	df['Titles']=df['Titles'].str.replace("</a>","")
	df["Titles"]=df["Titles"].str.lower()

	#print(df.head(266))

	links_lst=[]

	#Sorting out links with keywords
	searchfor = ['people', 'staff', 'search','directory','employees','phone','faces','profiles']
	
	for url_search in soup.find_all('a'):  
		urltext=url_search.get_text().replace(" ", "")
		#print(urltext.lower())
		for searchtext in searchfor:
			if searchtext in url_search.get_text().lower():
				try:
					if url_search.get('href') not in links_lst:          		
						links_lst.append(url_search.get('href'))
				except:
					print('invalid href')   
	
	x = "http"
	all_links=[]
	for stringurl in links_lst:		
		all_links.append(str(url) + str(stringurl))
	
	phlinks = []

	for nested_url in all_links:
		
		page = requests.get(nested_url)
		soup = BeautifulSoup(page.content,'html.parser')
		html_page = str(page.content)
		placeholder = "views-exposed-form-staff-people-search"
		if placeholder in html_page:
			phlinks=nested_url

	return phlinks


def seleniumDriver():
	options = Options()
	options.add_argument('--headless')
	options.add_argument('--no-sandbox')
	options.add_argument('--disable-dev-shm-usage')
	options.add_argument("start-maximized")
	#driver = webdriver.Chrome("C:/Users/michael/Downloads/chromedriver_win32/chromedriver.exe")
	#Or Use
	driver = webdriver.Chrome(chrome_options=options,executable_path='C:/wamp64/www/projects/scrap/chromedriver')
	return driver
	
def albhabetList():
	#Making a list of aa, ab, ac.......
	names = []
	for i in range(26):
		for j in range(26):
			x = chr(97+i) + chr(97+j)
			names.append(x)
	return names

def makeDataFrame():
	df = pd.DataFrame({'Name':[], 'Email': [], 'Organisation': [], 'Staff Type': [], 'Phone': []})
	return df
	
def getEmployees():	

	try:
		df=makeDataFrame()
		driver = seleniumDriver()
		urlsearch=seedurl()
		names= albhabetList()
		
		
		for name in names:

			driver.get(urlsearch)
			print(str(name) + " searcing start")
			page_no=1  
			driver.find_element_by_xpath('//*[@id="edit-name--2"]').send_keys(name)
			driver.find_element_by_xpath('//*[@id="edit-submit-staff--2"]').click()
			time.sleep(10)

		  #--------Data Extraction-------------
			while True:
				if page_no == 1:
					print("page no:" + str(page_no))
					page_no=page_no+1
				else:			
					
					try:
						element=driver.find_element_by_xpath('//span[contains(string(), "Next page")]')
						driver.execute_script("arguments[0].click();", element)					
						print("page no:" + str(page_no))
						page_no=page_no+1
						time.sleep(10)
					except:
						print("No more pages left")
						break
						
				elements1 = driver.find_elements_by_xpath("//td[@class='views-field views-field-views-conditional-field']")#name
				elements2 = driver.find_elements_by_xpath("//td[@class='views-field views-field-field-cpr-phone-number']")#phone
				elements3 = driver.find_elements_by_xpath("//td[@class='views-field views-field-field-cpr-category-tag']")#desg
				elements4 = driver.find_elements_by_xpath("//td[@class='views-field views-field-field-cpr-email']")#email
				elements5 = driver.find_elements_by_xpath("//td[@class='views-field views-field-field-cpr-group-name']")#org
				
				i=0
				for ele in elements1:	
				
					fname= ele.text
					email= elements4[i].text
					org= elements5[i].text
					desg= elements3[i].text
					phone= elements2[i].text
					i=i+1
					
					if email!="" or phone!="" :
						print(fname)
						df2 = pd.DataFrame({'Name': [fname], 'Email': [email], 'Organisation': [org], 'Staff Type': [desg], 'Phone': [phone]})
						df = df.append(df2, ignore_index=True, sort = False)  
		

	except:
		print("Error Found")
		return df	

	return df		
		
data=getEmployees()
print('\n')
print(data)
print('\n')
data.to_csv('NIST_data')