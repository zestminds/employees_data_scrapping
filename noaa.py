import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

#Seed URL
url = 'https://nsd.rdc.noaa.gov/'

def seedurl():

	#website Info : In Sheet wesbite : 9
	#https://www.noaa.gov/
	#National Oceanic and Atmospheric Administration

	#Seed URL 
	#url = 'https://nsd.rdc.noaa.gov/'
	url = 'https://www.noaa.gov/'
	website = requests.get(url)

	website_text = website.text
	links=[]
	soup = BeautifulSoup(website_text, 'lxml')
	print("Processing data. It may take sometime")
	for link in soup.find_all('a'):
	  links.append(str(link))
	
	#Processing the links from the seed URL webpage
	df = pd.DataFrame((links), columns =['Anchorlinks'])
	df[['Anchor', 'Links']] = df['Anchorlinks'].str.split('href="', 1, expand=True)
	df.drop(['Anchorlinks','Anchor'],axis = 'columns',inplace=True)
	df[['Links', 'Titles']] = df['Links'].str.split('">', 1, expand=True)
	df['Titles']=df['Titles'].str.replace("</a>","")
	df["Titles"]=df["Titles"].str.lower()

	#print(df.head(266))

	links_lst=[]

	#Sorting out links with keywords
	searchfor = ['people', 'staff', 'search','directory','employees','phone','faces','profiles']
	
	for url_search in soup.find_all('a'):  
		urltext=url_search.get_text().replace(" ", "")
		#print(urltext.lower())
		for searchtext in searchfor:
			if searchtext in url_search.get_text().lower():
				try:
					if url_search.get('href') not in links_lst:          		
						links_lst.append(url_search.get('href'))
				except:
					print('invalid href')   
		
	x = "http"
	all_links=[]
	for stringurl in links_lst:
		
		all_links.append(str(url) + str(stringurl))

	
	phlinks = []

	for nested_url in all_links:
		
		page = requests.get(nested_url)
		soup = BeautifulSoup(page.content,'html.parser')
		html_page = str(page.content)
		placeholder = "edit-submit-staff-directory"
		if placeholder in html_page:
			phlinks=nested_url

	return phlinks


def seleniumDriver():
	options = Options()
	options.add_argument('--headless')
	options.add_argument('--no-sandbox')
	options.add_argument('--disable-dev-shm-usage')
	options.add_argument("start-maximized")
	options.add_argument('--log-level=1')
	options.add_argument("--enable-popup-blocking")
	driver = webdriver.Chrome(chrome_options=options,executable_path='C:/wamp64/www/projects/scrap/chromedriver')
	return driver
	
def albhabetList():
	#Making a list of aa, ab, ac.......
	names = []
	for i in range(26):
		for j in range(26):
			x = chr(97+i) + chr(97+j)
			names.append(x)
	return names

def makeDataFrame():
	df = pd.DataFrame({'Name':[],'Email':[],'Line Office':[],'Phone': []})
	return df
	
def getEmployees():

	try:
		df=makeDataFrame()
		driver = seleniumDriver()
		urlsearch=seedurl()
		names= albhabetList()
		
		for name in names:

			driver.get(url)
			print(str(name) + " searcing start")
			page_no=1  
			driver.find_element(By.XPATH,'//*[@id="ln"]').send_keys(name)
			driver.find_element(By.XPATH,'//*[@id="buttonSearch"]').click()
			time.sleep(6)

		  #--------Data Extraction-------------
			while True:
				if page_no == 1:
					print("page no:" + str(page_no))
					page_no=page_no+1
				else:
					
					try:
						print("page no:" + str(page_no))
						page_no=page_no+1
						element=driver.find_element(By.XPATH,"//li[@class='PagedList-skipToNext']/a")
						driver.execute_script("arguments[0].click();", element)			
						time.sleep(10)
					except:
						print("No more pages left")
						break	
						
				elements1 = driver.find_elements(By.XPATH,'//td[@class="nameColumn"]')
				elements2 = driver.find_elements(By.XPATH,'//td[@class="emailColumn"]')
				elements3 = driver.find_elements(By.XPATH,'//td[@class="empTypeColumn"]')
				elements4 = driver.find_elements(By.XPATH,'//td[@class="phoneColumn"]')
				
				i=0
				for ele in elements1:

					try:
						fname= ele.text
					except:
						fname=""
					try:	
						email= elements2[i].text
					except:
						email=""
					try:
						org= elements3[i].text
					except:
						org=""
					try:
						phone= elements4[i].text
					except:
						phone=""
						
					print(fname)
					
					df2 = pd.DataFrame({'Name': [fname], 'Email': [email], 'Line Office': [org], 'Phone': [phone]})
					df = df.append(df2, ignore_index=True, sort = False)
					i=i+1
	except:
		print("Error Found")
		return df
	return df	
		
data=getEmployees()
print('\n')
print(data)
print('\n')
data.to_csv('NOAA_data')