import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

#Seed URL

#Finding URL's on home page and search for staff link
def seedurl():

	#website Info : In Sheet wesbite : 69
	#https://www.amnh.org/research/staff-directory
	#American Museum of Natural History

	#Seed URL 
	url='https://www.amnh.org'
	driver = seleniumDriver()
	driver.get(url)
	driver.implicitly_wait(60)
	driver.find_elements(By.CSS_SELECTOR, '.amnh-page-wrapper')

	soup = BeautifulSoup(driver.page_source, 'html.parser')	
	links=[]
	
	print("Processing data. It may take sometime")
	for link in soup.find_all('a'):		
		links.append(str(link))
	
	#Processing the links from the seed URL webpage
	df = pd.DataFrame((links), columns =['Anchorlinks'])
	df[['Anchor', 'Links']] = df['Anchorlinks'].str.split('href="', 1, expand=True)
	df.drop(['Anchorlinks','Anchor'],axis = 'columns',inplace=True)
	df[['Links', 'Titles']] = df['Links'].str.split('">', 1, expand=True)
	df['Titles']=df['Titles'].str.replace("</a>","")
	df["Titles"]=df["Titles"].str.lower()

	#print(df.head(266))

	links_lst=[]

	#Sorting out links with keywords
	searchfor = ['staff','research','people', 'search','directory','employees','phone','faces','profiles']
	
	for url_search in soup.find_all('a'):  
		urltext=url_search.get_text()		
		for searchtext in searchfor:
			if searchtext in url_search.get_text().lower():
				try:
					if url_search.get('href') not in links_lst:          		
						links_lst.append(url_search.get('href'))
				except:
					print('invalid href')   
	
	x = "http"
	all_links=[]
	phlinks = []
	for i in range(len(links_lst)):
		if x in links_lst[i]:
			continue

		else:
			all_links.append(str(url) + str(links_lst[i]))
	
	for nested_url in all_links:
		driver.get(nested_url)
		driver.implicitly_wait(20)
		try:
			driver.find_elements(By.CSS_SELECTOR, '.amnh-form amnh-standard-form')
			phlinks=nested_url		
		except:
			driver.close()	
	driver.quit()		
	return phlinks


def seleniumDriver():
	options = Options()
	options.add_argument('--headless')
	options.add_argument('--no-sandbox')
	options.add_argument('--disable-dev-shm-usage')
	options.add_argument("start-maximized")
	options.add_argument('--log-level=1')
	options.add_argument("--enable-popup-blocking")
	options.add_argument("test-type")
	options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36")
	#driver = webdriver.Chrome("C:/Users/michael/Downloads/chromedriver_win32/chromedriver.exe")
	#Or Use
	driver = webdriver.Chrome(chrome_options=options,executable_path='C:/wamp64/www/projects/scrap/chromedriver')
	return driver
	

def makeDataFrame():
	df = pd.DataFrame({'Name':[],'Email': [],'Phone': [],'Staff Type': [],'Division': []})
	return df
	
def getEmployees():	
	
	try:
		df=makeDataFrame()
		urlsearch=seedurl()		
		driver = seleniumDriver()
		driver.get(urlsearch)
		page_no=1  
		time.sleep(5)

	  #--------Data Extraction-------------
		while True:
			if page_no == 1:
				print("page no:" + str(page_no))
				page_no=page_no+1
			else:		
				#Paging 
				try:	
					#change Page tag
					print("page no:" + str(page_no))
					link = driver.find_element(By.LINK_TEXT,str(page_no))
					link.click()
					time.sleep(5)
					page_no=page_no+1
				except:
					print("No more pages left")
					break
						
			listoflinks=[]
			#Read data from employee list: finding all employee profile pages link	
			elements1 = driver.find_elements(By.XPATH,"//li[@class='mb-2 mb-s-2h']/h3/a")#name
			for ele in elements1:
				listoflinks.append(ele.get_property("href"))
					
			#open profile page one by one
			for ln in listoflinks:
				options = Options()
				options.add_argument('--headless')
				options.add_argument('--no-sandbox')
				options.add_argument('--disable-dev-shm-usage')
				options.add_argument("start-maximized")
				options.add_argument('--log-level=1')
				options.add_argument("--enable-popup-blocking")
				options.add_argument("test-type")
				options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36")
				#driver = webdriver.Chrome("C:/Users/michael/Downloads/chromedriver_win32/chromedriver.exe")
				#Or Use
				driver1 = webdriver.Chrome(chrome_options=options,executable_path='C:/wamp64/www/projects/scrap/chromedriver')		 
				driver1.get(ln)
				division=""
				email=""
				phone=""
				#Read data from employee Profile Page
				
				
				try:
					fname= driver1.find_element(By.XPATH,'//h1').text;
					print(fname)
					
				except:
					fname=""
				
				try:	
					email_text= driver1.find_element(By.XPATH,'//div[@class="amnh-content"]/dl/div/dd[1]').text					
					if "@" in email_text:
						email=email_text
					else:
						phone=email_text
				except:
					email=""
				
				try:	
					phone_text= driver1.find_element(By.XPATH,'//div[@class="amnh-content"]/dl').text
					list=phone_text.split('Phone:')
					phone=list[1]
				except:
					if phone=="":
						phone=""	
				
				try:
					staff_division= driver1.find_element(By.XPATH,'//h2').text
					staff_division_list=staff_division.split(",")
					staff_type=staff_division_list[0]
					try:
						if len(staff_division_list):
							division=staff_division_list[1]	
						if 2 in range(len(staff_division_list)):
							division=division+" ,"+str(staff_division_list[2])
					except:
						division=""						
				except:
					staff_type=""
					division=""			
				if email!="" or phone!="":
					df2 = pd.DataFrame({'Name': [fname], 'Email': [email],'Phone': [phone] ,'Staff Type': [staff_type],'Division': [division],})
					df = df.append(df2, ignore_index=True, sort = False)  
			
				driver1.close()
	except:
		print("Error found")
		return df
	return df	
		
data=getEmployees()
print('\n')
print(data)
print('\n')
data.to_csv('amnh_data.csv')